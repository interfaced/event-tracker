module.exports = {
	entry: './index.js',
	output: {
		path: './build',
		filename: 'event-tracker.browser.js',
		library: 'EventTracker',
		libraryTarget: 'this'
	}
};
