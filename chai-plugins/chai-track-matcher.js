var matcher = require('../lib/track-matcher');

module.exports = function(chai, utils) {
	var createDiffText = function(actual, expected, opt_keyword) {
		var keyword = opt_keyword || 'vs.';
		return [
			'',
			JSON.stringify(actual),
			keyword,
			JSON.stringify(expected)
		].join('\n');
	};

	chai.Assertion.addMethod('trackedEventNames', function(expectedTrack) {
		var tracker = this._obj;
		new chai.Assertion(tracker.getTrackedEvents).to.be.instanceOf(Function, 'Object is not EventTracker');
		var actualTrack = tracker.getTrackedEventNames();
		var success = matcher.isEventsMatchExpected(actualTrack, expectedTrack);
		this.assert(
			success,
			'Tracked events do not match instead of match.' + createDiffText(actualTrack, expectedTrack, 'does NOT match'),
			'Tracked events match instead of mismatch.' + createDiffText(actualTrack, expectedTrack, 'MATCHES'),
			expectedTrack,
			actualTrack,
			true
		);
	});
};
