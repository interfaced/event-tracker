/**
 * @param {Array.<EventTracker.TrackedEvent>} trackedEvents
 * @param {Array.<string>} expectedEvents
 * @return {boolean}
 */
isEventsMatchExpected = function(trackedEvents, expectedEvents) {
	for (var expectedIndex = 0, trackedIndex = 0; trackedIndex < trackedEvents.length; trackedIndex++) {
		if (isExpectedEventBatch(expectedEvents[expectedIndex - 1]) &&
			getExpectedEventName(expectedEvents[expectedIndex - 1]) === trackedEvents[trackedIndex]) {
			continue;
		}
		if (getExpectedEventName(expectedEvents[expectedIndex]) !== trackedEvents[trackedIndex]) {
			return false;
		}
		expectedIndex++;
	}
	return expectedIndex === expectedEvents.length;
};


/**
 * @param {string} expectedCode
 * @return {boolean}
 */
isExpectedEventBatch = function(expectedCode) {
	return expectedCode && expectedCode.split('...').length > 1;
};


/**
 * @param {string} expectedCode
 * @return {string}
 */
getExpectedEventName = function(expectedCode) {
	return expectedCode ? expectedCode.split('...')[0] : '';
};


module.exports = {
	isEventsMatchExpected: isEventsMatchExpected,
	isExpectedEventBatch: isExpectedEventBatch,
	getExpectedEventName: getExpectedEventName
};
