/**
 * @constructor
 */
EventTracker = function() {
	this._emitter = null;
	this._listeners = [];
	this.clearTrackedEvents();
};


// Public methods
// =====================================================================================================================


/**
 * @param {EventTracker.Emitter} emitter
 * @param {Array.<string>} eventsToTrack
 */
EventTracker.prototype.startTracking = function(emitter, eventsToTrack) {
	this._startTime = Date.now();
	this._emitter = emitter;
	eventsToTrack.map(function(eventName) {
		var listener = (function() {
			var time = Date.now();
			this._trackedEvents.push({
				name: eventName,
				args: Array.from(arguments),
				time: {
					absolute: time,
					relative: time - this._startTime
				}
			});
		}).bind(this);
		this._listeners[eventName] = listener;
		this._emitter.on(eventName, listener);
	}.bind(this));
};


/**
 *
 */
EventTracker.prototype.stopTracking = function() {
	Object.keys(this._listeners).forEach(function(eventName) {
		var listener = this._listeners[eventName];
		this._emitter.off(eventName, listener);
		delete this._listeners[eventName];
	}.bind(this));
	this._emitter = null;
};


/**
 * @return {boolean}
 */
EventTracker.prototype.isTracking = function() {
	return Boolean(this._emitter);
};


/**
 * Clear tracked events records
 */
EventTracker.prototype.clearTrackedEvents = function() {
	this._trackedEvents = [];
};


/**
 * @return {Array.<EventTracker.TrackedEvent>}
 */
EventTracker.prototype.getTrackedEvents = function(opt_filter) {
	var events = this._trackedEvents;
	if (opt_filter && typeof opt_filter === 'string') {
		return events.filter(function(event) {
			return event.name === opt_filter;
		})
	}
	return events;
};


/**
 * @return {Array.<string>}
 */
EventTracker.prototype.getTrackedEventNames = function() {
	return this._trackedEvents.map(function(eventRecord) {
		return eventRecord.name;
	});
};


// Properties
// =====================================================================================================================


/**
 * @type {EventTracker.Emitter}
 */
EventTracker.prototype._emitter;


/**
 * @type {Object.<Function>}
 */
EventTracker.prototype._listeners;


/**
 * @type {Array.<string>}
 */
EventTracker.prototype._eventsToTrack;


/**
 * @type {Array.<string>}
 */
EventTracker.prototype._trackedEvents;


/**
 * @type {number} timestamp
 */
EventTracker.prototype._startTime;


// Typedef
// =====================================================================================================================


/**
 * @typedef {{
 *     name: string,
 *     args: Array.<*>,
 *     time: {
 *         absolute: number,
 *         relative: number
 *     }
 * }}
 */
EventTracker.TrackedEvent;


/**
 * @typedef {IEventEmitter}
 */
EventTracker.Emitter;


// Export
// =====================================================================================================================


module.exports = EventTracker;
