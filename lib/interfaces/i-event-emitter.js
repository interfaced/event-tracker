/**
 * @interface
 */
IEventEmitter = function() {};


/**
 * @param {string} eventName
 * @param {Function} listener
 */
IEventEmitter.prototype.on = function(eventName, listener) {};


/**
 * @param {string} eventName
 * @param {Function} listener
 */
IEventEmitter.prototype.off = function(eventName, listener) {};


/**
 * @param {string} eventName
 * @param {Function} listener
 */
IEventEmitter.prototype.once = function(eventName, listener) {};


/**
 * @param {string} eventName
 * @param {...*} data
 */
IEventEmitter.prototype.emit = function(eventName, data) {};
