module.exports = function () {
	return {
		files: [
			'lib/**/*.js',
			'chai-plugins/**/*.js'
		],

		tests: [
			'test/**/*.mocha.js'
		],

		testFramework: 'mocha',

		env: {
			type: 'node'
		}
	};
};
