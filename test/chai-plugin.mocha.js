describe('Event tracker chai plugin', function() {
	var chai = require('chai');
	var expect = chai.expect;
	var plugin = require('../chai-plugins/chai-track-matcher');
	var EventTracker = require('../lib/event-tracker');

	describe('Connection', function() {
		it('connects to chai', function() {
			expect(function() {
				chai.use(plugin);
			}).not.to.throw()
		});
	});

	describe('Usage', function() {
		var tracker, emitter;
		var emitterBuilder = require('event-emitter');

		beforeEach(function() {
			tracker = new EventTracker();
			emitter = emitterBuilder({});
		});

		describe('Positive check', function() {
			it('passing test empty', function() {
				expect(tracker).trackedEventNames([]);
			});

			it('passing test', function() {
				tracker.startTracking(emitter, ['event-A', 'event-B']);
				emitter.emit('event-A');
				emitter.emit('event-B');
				expect(tracker).trackedEventNames([
					'event-A',
					'event-B'
				]);
			});

			it('failing test', function() {
				expect(function() {
					tracker.startTracking(emitter, ['event-A', 'event-B']);
					emitter.emit('event-A');
					expect(tracker).trackedEventNames([
						'event-A',
						'event-B'
					]);
				}).to.throw(chai.AssertionError, 'AssertionError: Tracked events do not match instead of match');
			});
		});

		describe('Negative check', function() {
			it('passing test', function() {
				tracker.startTracking(emitter, ['event-A', 'event-B']);
				emitter.emit('event-A');
				expect(tracker).not.trackedEventNames([
					'event-A',
					'event-B'
				]);
			});

			it('finling test', function() {
				expect(function() {
					tracker.startTracking(emitter, ['event-A', 'event-B']);
					emitter.emit('event-A');
					emitter.emit('event-B');
					expect(tracker).not.trackedEventNames([
						'event-A',
						'event-B'
					]);
				}).to.throw(chai.AssertionError, 'AssertionError: Tracked events match instead of mismatch');
			});
		});

	});
});
