describe('Track Matcher', function() {
	var chai = require('chai');
	chai.use(require('dirty-chai'));
	var expect = chai.expect;

	var matcher = require('../lib/track-matcher');

	describe('Syntax', function() {
		describe('Spread', function() {
			it('spread with ...', function() {
				expect(matcher.isExpectedEventBatch('event-A...')).to.be.true();
			});
			it('NOT spread without ...', function() {
				expect(matcher.isExpectedEventBatch('event-B')).to.be.false();
			});
		});
		describe('Name', function() {
			it('spread', function() {
				expect(matcher.getExpectedEventName('event-A...'))
					.equals('event-A');
			});
			it('common', function() {
				expect(matcher.getExpectedEventName('event-B'))
					.equals('event-B');
			});
		});
	});

	describe('Matching', function() {
		describe('Basic matching', function() {
			it('Two empty arrays - math', function() {
				expect(matcher.isEventsMatchExpected([], [])).to.be.true();
			});
			it('Two similar arrays - match', function() {
				expect(matcher.isEventsMatchExpected([
					'event-A',
					'event-B',
					'event-C'
				], [
					'event-A',
					'event-B',
					'event-C'
				])).to.be.true();
			});
			it('Different order - NOT match', function() {
				expect(matcher.isEventsMatchExpected([
					'event-A',
					'event-B',
					'event-C'
				], [
					'event-A',
					'event-C',
					'event-B'
				])).to.be.false();
			});
			it('Expected less event - NOT match', function() {
				expect(matcher.isEventsMatchExpected([
					'event-A',
					'event-B',
					'event-C'
				], [
					'event-A',
					'event-B'
				])).to.be.false();
			});
			it('Expected more event - NOT match', function() {
				expect(matcher.isEventsMatchExpected([
					'event-A',
					'event-B',
					'event-C'
				], [
					'event-A',
					'event-B',
					'event-C',
					'event-D'
				])).to.be.false();
			});
		});
		describe('Spread matching', function() {
			it('Spread in correct match -- match', function() {
				expect(matcher.isEventsMatchExpected([
					'event-A',
					'event-B', 'event-B', 'event-B',
					'event-C'
				], [
					'event-A',
					'event-B...',
					'event-C'
				])).to.be.true();
			});
			it('Misfit after spread -- NOT match', function() {
				expect(matcher.isEventsMatchExpected([
					'event-A',
					'event-B', 'event-B', 'event-B',
					'event-C'
				], [
					'event-A',
					'event-B...',
					'event-D'
				])).to.be.false();
			});
		});
	});
});
