describe('EventTracker', function() {
	var chai = require('chai');
	chai.use(require('chai-stats'));
	chai.use(require('dirty-chai'));
	var expect = chai.expect;

	var EventTracker = require('../lib/event-tracker');

	describe('Class', function() {
		it('class exported', function() {
			expect(EventTracker)
				.is.a('function');
		});
		it('default constructor', function() {
			expect(function() {
				return new EventTracker();
			}).not.to.throw();
		});
	});

	describe('Tracking', function() {
		var tracker, emitter;
		var emitterBuilder = require('event-emitter');

		beforeEach(function() {
			tracker = new EventTracker();
			emitter = emitterBuilder({});
		});
		afterEach(function() {
			tracker.stopTracking();
		});

		it('track events', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-B');
			emitter.emit('event-A');
			expect(tracker.getTrackedEventNames())
				.deep.equal([
					'event-B',
					'event-A'
				]);
		});
		it('skips unregistred events', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-A');
			emitter.emit('event-C');
			emitter.emit('event-B');
			expect(tracker.getTrackedEventNames())
				.deep.equal([
					'event-A',
					'event-B'
				]);
		});
		it('several same events make several records', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-A');
			emitter.emit('event-B');
			emitter.emit('event-A');
			emitter.emit('event-A');
			expect(tracker.getTrackedEventNames())
				.deep.equal([
					'event-A',
					'event-B',
					'event-A',
					'event-A'
				]);
		});

		it('can get events after tracking stopped', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-A');
			emitter.emit('event-A');
			emitter.emit('event-B');
			tracker.stopTracking();
			emitter.emit('event-A');
			expect(tracker.getTrackedEventNames())
				.deep.equal([
					'event-A',
					'event-A',
					'event-B'
				]);
		});
		it('can resume tracking', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-A');
			emitter.emit('event-A');
			tracker.stopTracking();
			emitter.emit('event-B');
			tracker.startTracking(emitter, ['event-A', 'event-C']);
			emitter.emit('event-C');
			expect(tracker.getTrackedEventNames())
				.deep.equal([
					'event-A',
					'event-A',
					'event-C'
				]);
		});
		it('can clear tracked events data', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-A');
			tracker.clearTrackedEvents();
			emitter.emit('event-B');
			expect(tracker.getTrackedEventNames())
				.deep.equal([
					'event-B'
				]);
		});

		describe('Event Time', function() {
			it('Event record has time fields: absolute and relative', function() {
				tracker.startTracking(emitter, ['event-A']);
				emitter.emit('event-A');
				tracker.stopTracking();
				var event = tracker.getTrackedEvents()[0];
				expect(event).have.property('time').that.is.an('object');
				expect(event.time).have.property('absolute').that.is.a('number');
				expect(event.time).have.property('relative').that.is.a('number').and.gte(0);
			});

			it('mark relative time from start tracking', function(done) {
				tracker.startTracking(emitter, ['event-A', 'event-B']);
				emitter.emit('event-A');
				setTimeout(function() {
					emitter.emit('event-B');
					tracker.stopTracking();
					var events = tracker.getTrackedEvents();
					expect(events[0].time.relative).almost.equal(0, -1);
					expect(events[1].time.relative).almost.equal(200, -1);
					done();
				}, 200)
			});
		});

		it('get events filtered by string event name', function() {
			tracker.startTracking(emitter, ['event-A', 'event-B']);
			emitter.emit('event-A');
			emitter.emit('event-B');
			emitter.emit('event-A');
			tracker.stopTracking();
			var eventsA = tracker.getTrackedEvents('event-A');
			expect(eventsA).length(2);
			eventsA.forEach(function(event) {
				expect(event.name === 'event-A');
			});
		});
	});
});
