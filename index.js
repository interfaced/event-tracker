var EventTracker = require('./lib/event-tracker');
var trackMatcher = require('./lib/track-matcher');
var globalTracker = new EventTracker();
var chaiPlugin = require('./chai-plugins/chai-track-matcher');

module.exports = {
	create: function() {
		return new EventTracker()
	},
	tracker: globalTracker,
	matcher: trackMatcher,
	plugin: {
		chai: chaiPlugin
	}
};
